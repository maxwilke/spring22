<?php



$rolls = [
    1,
    2,
    3,
    4,
    5,
    6
];

$pRoll1 = $rolls[mt_rand(0,5)];
$pRoll2 = $rolls[mt_rand(0,5)];

$cRoll1 = $rolls[mt_rand(0,5)];
$cRoll2 = $rolls[mt_rand(0,5)];
$cRoll3 = $rolls[mt_rand(0,5)];

$pTotal = ($pRoll1 + $pRoll2);
$cTotal = $cRoll1 + $cRoll2 + $cRoll3;

$winner = "";

if ($pTotal > $cTotal)
{
   $winner = "Player wins!";
}
elseif ($pTotal < $cTotal)
{
    $winner = "Computer wins!";
}
else
{
    $winner = "Its a Tie!";
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Dice Roll Game</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css" />

</head>

<body>

<header>
    <?php include('../template/header.php');?>
</header>

<nav>
    <ul>
        <?php include('../template/nav.php'); ?>
    </ul>
</nav>

<main>
    <h1>Dice Roll Game</h1>
    <br>
    <h2><?=$winner?></h2>
    <h3>Player Roll Total: <?=$pTotal?></h3>
    <img src="../img/dice_<?=$pRoll1?>.png"/>
    <img src="../img/dice_<?=$pRoll2?>.png"/>
    <br>
    <br>
    <h3>Computer Roll Total: <?=$cTotal?></h3>
    <img src="../img/dice_<?=$cRoll1?>.png"/>
    <img src="../img/dice_<?=$cRoll2?>.png"/>
    <img src="../img/dice_<?=$cRoll3?>.png"/>
</main>

<footer>
    <?php include('../template/footer.php'); ?>
</footer>

</body>

</html>
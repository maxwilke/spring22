<?php
session_start();

$answer = "Ask me a Question";
$question = "";

if (isset($_POST["txt_question"]))
{
    $question = $_POST["txt_question"];
}

$previous_question = "";
if (isset($_SESSION["previous_question"]))
{
    $previous_question = $_SESSION["previous_question"];
}

$responses = [
    "Ask again later",// 0
    "Yes",
    "No",
    "It appears to be so",
    "Reply is hazy, please try again",
    "Yes, definitely",
    "What is it you really want to know",
    "Outlook is good",
    "My sources say now",
    "Signs point to yes",
    "Do not count on it",
    "Cannot predict no",
    "As I see it, yes",
    "Better not tell you now",
    "Concentrate and ask again"// 14
];

if (empty($question))
{
    $answer = "Please ask a question";
}
else if (substr($question, -1) != "?")
{
    $answer = "Is that a statement or a question? Please use a question mark!";
}
else if ($previous_question == $question)
{
    $answer = "Please ask me a different question";
}
else
{
    $answer = $responses[mt_rand(0,14)];
    $_SESSION["previous_question"] = $question;
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Magic 8-Ball</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css" />

</head>

<body>

<header>
    <?php include('../template/header.php');?>
</header>

<nav>
    <ul>
        <?php include('../template/nav.php'); ?>
    </ul>
</nav>

<main>
    <h1>Magic 8-Ball</h1>

    <p>
        <marquee><?= $answer ?></marquee>
    </p>

    <form method="post" action="8ball.php">

        <p>
            <label for="txt_question">What do you wish to know?</label>
            <br>
            <input type="text" name="txt_question" id="txt_question" value="<?= $question ?>">
        </p>

        <input type="submit" value="Predict the Future">

    </form>

</main>

<footer>
    <?php include('../template/footer.php'); ?>
</footer>

</body>

</html>
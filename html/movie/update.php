<?php

if (isset($_GET['id']) && !empty($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) )  {

    $id = $_GET['id'];

    //--database stuff
    include ('../template/db_conn.php');

    try {
        $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
        $sql = $db->prepare("SELECT * FROM phpclass.movielist WHERE movie_id = :id");

        $sql->bindValue(":id", $id);
        $sql->execute();
        $row = $sql->fetch();

        if(!$row){
            header("Location: list.php");
        }





        if(is_null($row)){
            header("Location: list.php");
        }

        $title = $row['movie_title'];
        $rating = $row['movie_rating'];
    } catch (PDOException $e) {
        echo $e->getMessage();
        exit;
    }

} else {
    header("Location: list.php");
}

if (isset($_POST['movie_name']) && !empty($_POST['movie_name']) && isset($_POST['movie_rating']) && !empty($_POST['movie_rating']) && isset($_POST['movie_id']) && !empty($_POST['movie_id'])){
    //echo "<pre>"; print_r($_POST); echo "<pre>"; exit;

    $movie_title = $_POST['movie_name'];
    $movie_rating = $_POST['movie_rating'];
    $movie_id = $_POST['movie_id'];

    // DB stuff
    try {

        $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
        $sql = $db->prepare("UPDATE phpclass.movielist set movie_title = :Title, movie_rating = :Rating WHERE movie_id = :Id");
        $sql->bindValue(':Title', $movie_title);
        $sql->bindValue(':Rating', $movie_rating);
        $sql->bindValue(':Id', $movie_id);
        $sql->execute();

        //exit('DB Success!!!!!');

        header("Location:list.php?update=1");

    } catch (PDOException $e){
        echo "DB ERROR: " . $e->getMessage();
        exit;
    }
}
else if(isset($_POST) && !empty($_POST)) {
    $error = "Please ensure you have added both a title and rating before submitting a new movie.";
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Update Movie</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css" />

    <script type="text/javascript">

        function DeleteMovie(moviename, id)
        {

          if(confirm("Do you really want to delete " + moviename + "?")){
              document.location.href = "delete.php?id="+id;
          }

        }

    </script>

</head>

<body>

<header>
    <?php include('../template/header.php');?>
</header>

<nav>
    <ul>
        <?php include('../template/nav.php'); ?>
    </ul>
</nav>

<main>
    <h1>Update Movie</h1>

    <?php if(isset($_GET['success'])) {?>
    <p class="success">Movie Update/Added Successfully!</p>
    <?php }?>

    <form method="post">
        <input type="hidden" name="movie_id" id="movie_id" value="<?= $id ?>">

        <?php if(isset($error)){ ?>
        <p class="error">Please enter both a movie and rating!</p>
        <?php } ?>

        <table border="1" width="80%">

            <tr height="100">
                <th colspan="2">Update Movie</th>
            </tr>

            <tr height="50">
                <th>Movie Name</th>
                <td><input type="text" name="movie_name" id="movie_name" value="<?= $title ?>"/></td>
            </tr>
            <tr height="50">
                <th>Movie Rating</th>
                <td><input type="text" name="movie_rating" id="movie_rating" value="<?= $rating ?>"/></td>
            </tr>
            <tr height="100">
                <td colspan="2">
                    <input type="submit" name="movie_submit" id="movie_submit" value="Update Movie"/>
                    <input type="button" name="movie_delete" id="movie_delete" value="Delete Movie"
                     onclick="DeleteMovie('<?= $title ?>','<?= $id ?>')"
                     />
                </td>
            </tr>

        </table>

    </form>
</main>

<footer>
    <?php include('../template/footer.php'); ?>
</footer>

</body>

</html>
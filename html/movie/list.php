<?php

include('../template/db_conn.php');

try{

    $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
    $sql = $db->prepare("SELECT * FROM phpclass.movielist;");
    $sql->execute();
    $rows = $sql->fetchAll();


    //echo"<pre>";
    //print_r($rows);
    //echo"<pre>";
    //exit;

} catch(PDOException $e){
    echo $e->getMessage();
    exit;
}


?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Movie List</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css" />

</head>

<body>

<header>
    <?php include('../template/header.php');?>
</header>

<nav>
    <ul>
        <?php include('../template/nav.php'); ?>
    </ul>
</nav>

<main>


    <?php if (isset($_GET['delete']) && $_GET['delete'] == 1) { ?>
        <p class="success">Movie Deleted!</p>
    <?php } else if(isset($_GET['delete']) && $_GET['delete'] == 0) { ?>
    <p class="success">Movie could not be deleted!</p>
    <?php } ?>

    <?php if (isset($_GET['update']) && $_GET['update'] == 1) { ?>
        <p class="success">Movie updated!</p>
    <?php } else if(isset($_GET['update']) && $_GET['update'] == 0) { ?>
    <p class="success">Movie could not be updated!</p>
    <?php } ?>

    <?php if($_GET['success']) { ?>
        <p class="success">New Movie Added!</p>
    <?php } ?>


    <h1>My movie list</h1>

    <table border="1" width="80%">

        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Rating</th>
        </tr>

        <?php foreach ($rows as $movie): ?>
        <tr>
            <td><?= $movie['movie_id'] ?></td>
            <td><a href="update.php?id=<?= $movie['movie_id']?>"><?= $movie['movie_title']?></a></td>
            <td><?= $movie['movie_rating']?></td>
        </tr>
        <?php endforeach; ?>
    </table>

    <p>
        <a href="add.php">Add New Movie</a>
    </p>
</main>

<footer>
    <?php include('../template/footer.php'); ?>
</footer>

</body>

</html>
<?php

if (isset($_POST['movie_name']) && !empty($_POST['movie_name']) && isset($_POST['movie_rating']) && !empty($_POST['movie_rating'])){
    //echo "<pre>"; print_r($_POST); echo "<pre>"; exit;

    $title = $_POST['movie_name'];
    $rating = $_POST['movie_rating'];

    // DB stuff

    include('../template/db_conn.php');
    try {

        $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
        $sql = $db->prepare("INSERT INTO phpclass.movielist(movie_title, movie_rating) VALUE (:Title, :Rating)");
        $sql->bindValue(':Title', $title);
        $sql->bindValue(':Rating', $rating);
        $sql->execute();

        //exit('DB Success!!!!!');

        header("Location:list.php?success=1");

    } catch (PDOException $e){
        echo "DB ERROR: " . $e->getMessage();
        exit;
    }
}
else if(isset($_POST) && !empty($_POST)) {
    $error = "Please ensure you have added both a title and rating before submitting a new movie.";
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Add Movie</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css" />

</head>

<body>

<header>
    <?php include('../template/header.php');?>
</header>

<nav>
    <ul>
        <?php include('../template/nav.php'); ?>
    </ul>
</nav>

<main>
    <h1>Add Movie</h1>

    <form method="post">

        <?php if(isset($error)){ ?>
        <p class="error">Please enter both a movie and rating!</p>
        <?php } ?>

        <table border="1" width="80%">

            <tr height="100">
                <th colspan="2">Add New Movie</th>
            </tr>

            <tr height="50">
                <th>Movie Name</th>
                <td><input type="text" name="movie_name" id="movie_name" /></td>
            </tr>
            <tr height="50">
                <th>Movie Rating</th>
                <td><input type="text" name="movie_rating" id="movie_rating" /></td>
            </tr>
            <tr height="100">
                <td colspan="2">
                    <input type="submit" name="movie_submit" id="movie_submit" value="Add Movie"/>
                </td>
            </tr>

        </table>

    </form>
</main>

<footer>
    <?php include('../template/footer.php'); ?>
</footer>

</body>

</html>
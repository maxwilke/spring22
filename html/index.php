<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Max's Homepage</title>
    <link rel="stylesheet" type="text/css" href="css/base.css" />

</head>

<body>

<header>
    <?php include('template/header.php');?>
</header>

<nav>
    <ul>
        <?php include('template/nav.php'); ?>
    </ul>
</nav>

<main>
    <img src="img/baseball.jpg" alt="Max Image"/>
    <p>Vivamus libero leo, lacinia lobortis iaculis ac, feugiat vitae nisi. Sed molestie ornare urna, ac convallis lorem venenatis non. Cras euismod pulvinar scelerisque. Ut viverra blandit vulputate. Aliquam consectetur felis vel justo tempor cursus. Integer sollicitudin molestie quam ac auctor. Quisque vel sapien id mauris mattis varius ac vitae nulla. Suspendisse eget consectetur sem. Nam ac sapien id ligula ornare ornare vel et ante. Vestibulum est neque, gravida quis mi id, luctus condimentum ligula.</p>
</main>

<footer>
   <?php include('template/footer.php'); ?>
</footer>

</body>

</html>
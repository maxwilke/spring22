<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Loops</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css" />

</head>

<body>

<header>
    <?php include('../template/header.php');?>
</header>

<nav>
    <ul>
        <?php include('../template/nav.php'); ?>
    </ul>
</nav>

<main>
    <h1>Loops and String Functions Demo</h1>


    <h3>Basic Vars and Stuff</h3>
    <?php
        $number = 100;
        print $number;
        echo "<br><strong>".$number."</strong>";
        echo "<br><strong>";
        echo $number;
        echo "</strong";

        echo "<br><strong>$number</strong>";
        echo '<br><strong>$number</strong>';
        echo "<br><strong>{$number}</strong>";

        $result = "<br><strong>";
        $result .= $number;
        $result .= "</strong>";
        echo $result;
    ?>
    <h3>Math</h3>
    <?php

    $number_1 = 100;
    $number_2 = "50";
    $number_3 = 50;

    $result_1 = $result_1 + $number_2;
    $result_2 = $number_1 + $number_3;

    echo "<br>".$result_1;
    echo "<br>".$result_2;



    ?>
    <h3>While Loop</h3>

    <?php

    $i = 1;
    while ($i < 7)
    {
       echo "<h$i>Howdy World!</$i>";
        $i++;

    }


    ?>

    <h3>Do While Loop</h3>

    <?php

    $i = 6;
    do
    {
        echo "<h$i>Howdy World!</$i>";
        $i--;

    }
    while ($i >0);

    ?>

    <h3>For Loop</h3>
    <?php
    for ($i=1;$i<7;$i++)
    {
        echo "<h$i>Hello World</h$i>";
    }
    ?>

    <?php
    echo "<br /><br /><hr /><br />";
    $Full_Name = "Doug Smith";
    $Position = strpos($Full_Name, " ");

    echo $Position;
    echo "<br /><br /><hr /><br />";

    echo $Full_Name;
    echo "<br />";

    $Full_Name = strtoupper($Full_Name);
    echo $Full_Name;

    echo "<br /><br /><hr /><br />";

    echo $Full_Name;
    echo "<br />";

    $Full_Name = strtolower($Full_Name);
    echo $Full_Name;

    echo "<br /><br /><hr /><br />";

    $nameParts = explode(" ", $Full_Name);
    echo $nameParts[0];
    echo "<br />";
    echo $nameParts[1];

    ?>


</main>

<footer>
    <?php include('../template/footer.php'); ?>
</footer>

</body>

</html>
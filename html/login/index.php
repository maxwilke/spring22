<?php

session_start();

if(isset($_SESSION['UID'])){
    unset($_SESSION['UID']);
    $error = 'Admin user logged out';
}



if (isset($_POST['txt_email']) && !empty($_POST['txt_email']) && isset($_POST['txt_password']) && !empty($_POST['txt_password'])){


    $user_email = $_POST['txt_email'];
    $user_password = $_POST['txt_password'];


    include('../template/db_conn.php');

    try{

        $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
        $sql = $db->prepare("SELECT member_id, password, member_key, role_id FROM phpclass.member_login WHERE email = :Email");
        $sql->bindValue(':Email', $user_email);
        $sql->execute();
        $rows = $sql->fetch();


        if($rows!=null){


        $hashedPassword = md5($user_password . $rows["member_key"]);

            //echo $hashedPassword;

        if($hashedPassword == $rows["password"]){
            $_SESSION['UID'] = $rows["member_id"];
            $_SESSION['Role'] = $rows['role_id'];
            if($rows["role_id"]==1){
                //echo $password;
                header("Location:admin.php");
            } else{
                header("Location:member.php");
                }
            }else{
            $error = 'Wrong Username';
        }
        }else{
            $error = 'Wrong Password';
        }

    } catch(PDOException $e){
        echo $e->getMessage();
        exit;
    }







    /*if($user_email == strtolower('admin') && $user_password == strtolower('p@ss')) {
        $_SESSION['UID'] = 1;
        header("Location:admin.php");
    } elseif ($user_email == 'member' && $user_password == 'p@ss'){

        header("Location:member.php");
    } else {
        $error = 'Wrong username or password';
    }*/

}
else if(isset($_POST) && !empty($_POST)) {
    $error = "Wrong";
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css" />



</head>

<body>

<header>
    <?php include('../template/header.php');?>
</header>

<nav>
    <ul>
        <?php include('../template/nav.php'); ?>
    </ul>
</nav>

<main>
    <h1>Max's Website</h1>
    <form method="post">


        <?php if(isset($error)){ ?>
            <p class="error"><?=$error?></p>
        <?php } ?>

        <table border="1" width="80%">

            <tr height="100">
                <th colspan="2">Login</th>
            </tr>

            <tr height="50">
                <th>Email</th>
                <td><input type="text" name="txt_email" id="txt_email" value="<?= $user_email ?>"/></td>
            </tr>
            <tr height="50">
                <th>Password</th>
                <td><input type="password" name="txt_password" id="txt_password" value="<?= $user_password ?>"/></td>
            </tr>
            <tr height="100">
                <td colspan="2">
                    <input type="submit" name="login_submit" id="login_submit" value="Login"/>
                </td>
            </tr>

        </table>

    </form>
</main>

<footer>
    <?php include('../template/footer.php'); ?>
</footer>

</body>

</html>
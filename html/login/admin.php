<?php
session_start();

$key = sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));


if($_SESSION['Role']!=1) {
    header('Location:index.php');
}

//-- data validations

if (isset($_POST['user_submit'])) {

    if(isset($_POST['txt_name']) && !empty($_POST['txt_name'])){
        $fullname = $_POST['txt_name'];
    } else {
        $error[] = "Name is required";
    }
    if(isset($_POST['txt_email']) && !empty($_POST['txt_email'])){
        $email = $_POST['txt_email'];
    } else {
        $error[] = "Email is required";
    }
    if(isset($_POST['txt_password']) && !empty($_POST['txt_password'])){
        $password = $_POST['txt_password'];
    } else {
        $error[] = "Password is required";
    }

    if ($password != $_POST['txt_verify_password']){
        $error[] = "Password fields must match";
    } else {
        $password_verify = $_POST['txt_verify_password'];
    }


    if(isset($_POST['txt_role']) && !empty($_POST['txt_role'])){
        $role = $_POST['txt_role'];
    } else {
        $error[] = "Role is required";
    }

    if (empty($error)){
        include ('../template/db_conn.php');
        try {
            $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
            $sql = $db->prepare("INSERT INTO phpclass.member_login (name, email, role_id, password, member_key)
        VALUE (:Name, :Email, :Roleid, :Password, :Key)");
            $sql->bindValue(':Name', $fullname);
            $sql->bindValue(':Email', $email);
            $sql->bindValue(':Roleid', $role);
            $sql->bindValue(':Password', md5($password . $key));
            $sql->bindValue(':Key', "$key");
            $sql->execute();

        } catch(PDOException $e) {
            echo $e->getMessage();
            exit;
        }
        $error[] = "User Registered";
        unset($fullname, $email, $password, $password_verify, $role);
    }
}





?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Login Admin</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css" />

</head>

<body>

<header>
    <?php include('../template/header.php');?>
</header>

<nav>
    <ul>
        <?php include('../template/nav.php'); ?>
    </ul>
</nav>

<main>
    <h1>Login Admin</h1>

    <form method="post">


        <?php if(isset($error) && !empty($error)){ ?>
            <?php foreach($error as $e){ ?>
                <p class="error"><?= $e; ?></p>
                <?php } ?>
        <?php } ?>

        <table border="1" width="80%">

            <tr height="100">
                <th colspan="2">User Registration</th>
            </tr>

            <tr height="50">
                <th>Full Name</th>
                <td><input type="text" name="txt_name" id="txt_name" value="<?= $fullname ?>"/></td>
            </tr>

            <tr height="50">
                <th>Email</th>
                <td><input type="text" name="txt_email" id="txt_email" value="<?= $email ?>"/></td>
            </tr>
            <tr height="50">
                <th>Role</th>
                <td><select name="txt_role" id="txt_role">
                        <option
                            value="3"
                            <?php if($role == '3'){echo "selected";} ?>
                        >Member</option>
                        <option
                            value="1"
                            <?php if($role == '2'){echo "selected";} ?>
                        >Admin</option>
                        <option
                            value="2"
                            <?php if($role == '2'){echo "selected";} ?>
                        >Operator</option>
                    </select></td>
            </tr>
            <tr height="50">
                <th>Password</th>
                <td><input type="password" name="txt_password" id="txt_password" value="<?= $password ?>"/></td>
            </tr>
            <tr height="50">
                <th>Verify Password</th>
                <td><input type="password" name="txt_verify_password" id="txt_verify_password" value="<?= $password_verify ?>"/></td>
            </tr>
            <tr height="100">
                <td colspan="2">
                    <input type="submit" name="user_submit" id="user_submit" value="Create User"/>
                </td>
            </tr>

        </table>

    </form>
</main>

<footer>
    <?php include('../template/footer.php'); ?>
</footer>

</body>

</html>
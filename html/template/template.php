<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Max's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css" />

</head>

<body>

<header>
    <?php include('../template/header.php');?>
</header>

<nav>
    <ul>
        <?php include('../template/nav.php'); ?>
    </ul>
</nav>

<main>
    <h1>Template</h1>
</main>

<footer>
    <?php include('../template/footer.php'); ?>
</footer>

</body>

</html>
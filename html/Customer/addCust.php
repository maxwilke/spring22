<?php if (isset($_POST['CustomerID']) && !empty($_POST['CustomerID']) && isset($_POST['FirstName']) && !empty($_POST['FirstName']) && isset($_POST['LastName']) && !empty($_POST['LastName']) && isset($_POST['Address']) && !empty($_POST['Address'])&& isset($_POST['City']) && !empty($_POST['City']) && isset($_POST['State']) && !empty($_POST['State'])&& isset($_POST['Zip']) && !empty($_POST['Zip']) && isset($_POST['Phone']) && !empty($_POST['Phone']) && isset($_POST['Email']) && !empty($_POST['Email']) && isset($_POST['Password']) && !empty($_POST['Password'])){
//echo "<pre>"; print_r($_POST); echo "<pre>"; exit;
    $key = sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));

    $CustID = $_POST['CustomerID'];
    $FName = $_POST['FirstName'];
    $LName = $_POST['LastName'];
    $Address = $_POST['Address'];
    $City = $_POST['City'];
    $State = $_POST['State'];
    $Zip = $_POST['Zip'];
    $Phone = $_POST['Phone'];
    $Email = $_POST['Email'];
    $Password = $_POST['Password'];

    // DB stuff

    include('../template/db_conn.php');
    try {

        $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
        $sql = $db->prepare("INSERT INTO phpclass.CustomerTable(CustomerID, FirstName, LastName, Address, City, State, Zip, Phone, Email, Password, cust_key) VALUE (:CustID, :FName, :LName, :Address, :City, :State, :Zip, :Phone, :Email, :Password, :cust_key)");
        $sql->bindValue(':CustID', $CustID);
        $sql->bindValue(':FName', $FName);
        $sql->bindValue(':LName', $LName);
        $sql->bindValue(':Address', $Address);
        $sql->bindValue(':City', $City);
        $sql->bindValue(':State', $State);
        $sql->bindValue(':Zip', $Zip);
        $sql->bindValue(':Phone', $Phone);
        $sql->bindValue(':Email', $Email);
        $sql->bindValue(':Password', md5($Password . $key));
        $sql->bindValue(':cust_key', "$key");
        $sql->execute();

        //exit('DB Success!!!!!');

        header("Location:CustListing.php?success=1");

    } catch (PDOException $e){
        echo "DB ERROR: " . $e->getMessage();
        exit;
    }
}
else if(isset($_POST) && !empty($_POST)) {
    $error = "Please ensure you have all fields filled in.";
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Add Customer</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css" />

</head>

<body>

<header>
    <?php include('../template/header.php');?>
</header>

<nav>
    <ul>
        <?php include('../template/nav.php'); ?>
    </ul>
</nav>

<main>
    <h1>Add Customer</h1>

    <form method="post">

        <?php if(isset($error)){ ?>
            <p class="error"><?= $error ?></p>
        <?php } ?>

        <table border="1" width="80%">

            <tr height="100">
                <th colspan="2">Add New Customer</th>
            </tr>

            <tr height="50">
                <th>Customer ID</th>
                <td><input type="text" name="CustomerID" id="CustomerID" /></td>
            </tr>
            <tr height="50">
                <th>First Name</th>
                <td><input type="text" name="FirstName" id="FirstName" /></td>
            </tr>
            <tr height="50">
                <th>Last Name</th>
                <td><input type="text" name="LastName" id="LastName" /></td>
            </tr>
            <tr height="50">
                <th>Address</th>
                <td><input type="text" name="Address" id="Address" /></td>
            </tr>
            <tr height="50">
                <th>City</th>
                <td><input type="text" name="City" id="City" /></td>
            </tr>
            <tr height="50">
                <th>State</th>
                <td><input type="text" name="State" id="State" /></td>
            </tr>
            <tr height="50">
                <th>Zip</th>
                <td><input type="text" name="Zip" id="Zip" /></td>
            </tr>
            <tr height="50">
                <th>Phone</th>
                <td><input type="text" name="Phone" id="Phone" /></td>
            </tr>
            <tr height="50">
                <th>Email</th>
                <td><input type="text" name="Email" id="Email" /></td>
            </tr>
            <tr height="50">
                <th>Password</th>
                <td><input type="password" name="Password" id="Password" /></td>
            </tr>
            <tr height="100">
                <td colspan="2">
                    <input type="submit" name="cust_submit" id="cust_submit" value="Add Customer"/>
                </td>
            </tr>

        </table>

    </form>
</main>

<footer>
    <?php include('../template/footer.php'); ?>
</footer>

</body>

</html>
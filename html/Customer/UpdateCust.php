<?php

if (isset($_GET['id']) && !empty($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) )  {

    $id = $_GET['id'];

    //--database stuff
    include ('../template/db_conn.php');

    try {
        $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
        $sql = $db->prepare("SELECT * FROM phpclass.CustomerTable WHERE CustomerID = :id");

        $sql->bindValue(":id", $id);
        $sql->execute();
        $row = $sql->fetch();

        if(!$row){
            header("Location: CustListing.php");
        }





        if(is_null($row)){
            header("Location: CustListing.php");
        }

        $CustID = $_POST['CustomerID'];
        $FirstName = $_POST['FirstName'];
        $LastName = $_POST['LastName'];
        $Address = $_POST['Address'];
        $City = $_POST['City'];
        $State = $_POST['State'];
        $Zip = $_POST['Zip'];
        $Phone = $_POST['Phone'];
        $Email = $_POST['Email'];
        $Password = $_POST['Password'];
        $PasswordV = $_POST['PasswordV'];
    } catch (PDOException $e) {
        echo $e->getMessage();
        exit;
    }

} else {
    header("Location: CustListing.php");
}

if (
    isset($_POST['FirstName']) && !empty($_POST['FirstName'])
    && isset($_POST['LastName']) && !empty($_POST['LastName'])
    && isset($_POST['Address']) && !empty($_POST['Address'])
    && isset($_POST['City']) && !empty($_POST['City'])
        && isset($_POST['State']) && !empty($_POST['State'])
        && isset($_POST['Zip']) && !empty($_POST['Zip'])
        && isset($_POST['Phone']) && !empty($_POST['Phone'])
        && isset($_POST['Email']) && !empty($_POST['Email'])
        && isset($_POST['Password']) && !empty($_POST['Password'])
        && isset($_POST['PasswordV']) && !empty($_POST['PasswordV']
    ))
{

    $CustID = $_POST['CustomerID'];
    $FirstName = $_POST['FirstName'];
    $LastName = $_POST['LastName'];
    $Address = $_POST['Address'];
    $City = $_POST['City'];
    $State = $_POST['State'];
    $Zip = $_POST['Zip'];
    $Phone = $_POST['Phone'];
    $Email = $_POST['Email'];
    $Password = $_POST['Password'];
    $PasswordV = $_POST['PasswordV'];

    // DB stuff
    try {

        $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
        $sql = $db->prepare("UPDATE phpclass.CustomerTable set 
 FirstName = :FirstName, 
 LastName = :LastName, 
 Address = :Address, 
 City = :City, 
 State = :State, 
 Zip = :Zip, 
 Phone = :Phone, 
 Email = :Email, 
 Password = :Password 
 WHERE CustomerID = :id");
        $sql->bindValue(':FirstName', $FirstName);
        $sql->bindValue(':LastName', $LastName);
        $sql->bindValue(':Address', $Address);
        $sql->bindValue(':City', $City);
        $sql->bindValue(':State', $State);
        $sql->bindValue(':Zip', $Zip);
        $sql->bindValue(':Phone', $Phone);
        $sql->bindValue(':Email', $Email);
        $sql->bindValue(':Password', $Password);
        $sql->bindValue(':id', $CustID);
        //$sql->bindValue(':PasswordV', $PasswordV);
        $sql->execute();

        //exit('DB Success!!!!!');

        header("Location:CustListing.php?update=1");

    } catch (PDOException $e){
        echo "DB ERROR: " . $e->getMessage();
        exit;
    }
}
else if(isset($_POST) && !empty($_POST)) {
    $error = "Please ensure you have added all information.";
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Update Customer</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css" />

    <script type="text/javascript">

        function DeleteCustomer(id, FirstName, LastName)
        {

            if(confirm("Do you really want to delete " + FirstName + " " + LastName + "?")){
                document.location.href = "CustDelete.php?id="+id;
            }

        }

    </script>

</head>

<body>

<header>
    <?php include('../template/header.php');?>
</header>

<nav>
    <ul>
        <?php include('../template/nav.php'); ?>
    </ul>
</nav>

<main>
    <h1>Update Movie</h1>

    <?php if(isset($_GET['success'])) {?>
        <p class="success">Customer Update/Added Successfully!</p>
    <?php }?>

    <form method="post">
        <input type="hidden" name="CustomerID" id="CustomerID" value="<?= $id ?>">

        <?php if(isset($error)){ ?>
            <p class="error"><?= $error ?></p>
        <?php } ?>

        <table border="1" width="80%">

            <tr height="100">
                <th colspan="2">Update Customer</th>
            </tr>

            <tr height="50">
                <th>First Name:</th>
                <td><input type="text" name="FirstName" id="FirstName" value="<?= $row['FirstName'] ?>"/></td>
            </tr>
            <tr height="50">
                <th>Last Name:</th>
                <td><input type="text" name="LastName" id="LastName" value="<?= $row['LastName'] ?>"/></td>
            </tr>
            <tr height="50">
                <th>Phone Number:</th>
                <td><input type="text" name="Phone" id="Phone" value="<?= $row['Phone'] ?>"/></td>
            </tr>
            <tr height="50">
                <th>Email:</th>
                <td><input type="email" name="Email" id="Email" value="<?= $row['Email'] ?>"/></td>
            <tr height="50">
                <th>Address:</th>
                <td><input type="text" name="Address" id="Address" value="<?= $row['Address'] ?>"/></td>
            </tr>
            <tr height="50">
                <th>City:</th>
                <td><input type="text" name="City" id="City" value="<?= $row['City'] ?>"/></td>
            <tr height="50">
                <th>Zip Code:</th>
                <td><input type="text" name="Zip" id="Zip" value="<?= $row['Zip'] ?>"/></td>
            </tr>
            <tr height="50">
                <th>State:</th>
                <td><input type="text" name="State" id="State" value="<?= $row['State'] ?>"/></td>
            <tr height="50">
                <th>Password:</th>
                <td><input type="password" name="Password" id="Password" value="<?= $row['Password'] ?>"/></td>
            </tr>
            <tr height="50">
                <th>Password Verification:</th>
                <td><input type="password" name="PasswordV" id="PasswordV" value="<?= $row['Password'] ?>"/></td>
            </tr>
            <tr height="100">
                <td colspan="2">
                    <input type="submit" name="customer_submit" id="customer_submit" value="Update customer"/>
                    <input type="button" name="customer_delete" id="customer_delete" value="Delete customer"
                           onclick="DeleteCustomer('<?= $id ?>','<?= $row['FirstName'] ?>','<?= $row['LastName'] ?>')"
                    />
                </td>
            </tr>

        </table>

    </form>
</main>

<footer>
    <?php include('../template/footer.php'); ?>
</footer>

</body>

</html>
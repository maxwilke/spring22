<?php

include('../template/db_conn.php');

try{

    $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
    $sql = $db->prepare("SELECT * FROM phpclass.CustomerTable;");
    $sql->execute();
    $rows = $sql->fetchAll();


    //echo"<pre>";
    //print_r($rows);
    //echo"<pre>";
    //exit;

} catch(PDOException $e){
    echo $e->getMessage();
    exit;
}


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Customer Listing</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css" />

</head>

<body>

<header>
    <?php include('../template/header.php');?>
</header>

<nav>
    <ul>
        <?php include('../template/nav.php'); ?>
    </ul>
</nav>

<main>
    <h1>Customer Listing</h1>

    <table border="1" width="80%">

        <tr>
            <th>Customer ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Zip</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Password</th>
        </tr>

        <?php foreach ($rows as $customer): ?>
            <tr>
                <td><?= $customer['CustomerID'] ?></td>
                <td><a href="UpdateCust.php?id=<?= $customer['CustomerID'] ?>">
                        <?= $customer['FirstName']?>
                        <?= $LastName['LastName'] ?>
                        <?= $Address['Address'] ?>
                        <?= $City['City'] ?>
                        <?= $State['State'] ?>
                        <?= $Zip['Zip'] ?>
                        <?= $Phone['Phone'] ?>
                        <?= $Email['Email'] ?></a></td>
                <td><?= $customer['LastName'] ?></td>
                <td><?= $customer['Address'] ?></td>
                <td><?= $customer['City'] ?></td>
                <td><?= $customer['State'] ?></td>
                <td><?= $customer['Zip'] ?></td>
                <td><?= $customer['Phone'] ?></td>
                <td><?= $customer['Email'] ?></td>
                <td><?= $customer['Password'] ?></td>
            </tr>
        <?php endforeach; ?>
    </table>

    <p>
        <a href="addCust.php">Add New Customer</a>
    </p>
</main>

<footer>
    <?php include('../template/footer.php'); ?>
</footer>

</body>

</html>
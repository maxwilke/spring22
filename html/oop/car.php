<?php

class Car{
    /**
     * The color of the vehicle
     *
     * @var string
     */
    public $color;

    /**
     * Chevy, Dodge
     *
     * @var string
     */
    public $make;
    /**
     * Corvette
     *
     * @var string
     */
    public $model;
    /**
     * 2021, 2022
     *
     * @var int
     */
    public $year;
    /**
     * Parked, forward, reverse
     *
     * @var string
     */
    public $status;

    /**
     * Car constructor.
     */
    function __construct()
{
    $this->status = "parked";
}

    /**
     * put the car in drive
     */
function drive(){
    echo "The car is moving forward.<br/><br/>";
    $this->status = "drive";
    }

    /**
     * put the car in reverse
     */
    function reverse(){
        echo "The car is moving backwards.<br/><br/>";
        $this->status = "reverse";
    }

    /**
     * put the car in park
     */
    function park(){
        echo "The car is now parked.<br/><br/>";
        $this->status = "parked";
    }

    /**
     * Set the color of the car
     *
     * @param string $color the color of the car (red, blue, etc.)
     */
    function setColor($color){
        $this->color = $color;

        $valid_colors = ["red", "green", "blue"];
    }

    /**
     *
     * @return string
     */
    function getColor($color){
        return $this->color;
    }





}//--end of class

$my_first_car = new Car();

$my_first_car->setColor("Green");

$my_first_car->make = "Dodge";

$my_first_car->model = "Shadow";

$my_first_car->year = 1994;

echo "My first car was $my_first_car->year $my_first_car->model $my_first_car->make<br/><br/> ";

$my_first_car->drive();

$my_first_car->reverse();

$my_first_car->park();
?>
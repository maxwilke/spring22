<?php
/*
 * This is a countdown timer
 * 5-21-22
 */

$secPerMin = 60;
$secPerHour = 60 * $secPerMin;
$secPerDay = 24 * $secPerHour;
$secPerYear = 365 * $secPerDay;

//Current time
$now = time();

//School end time
$schoolEnd = mktime(12,0,0, 5, 21, 2022);

//number of seconds between now and then
$seconds = $schoolEnd - $now;

$years = floor($seconds/$secPerYear);
$seconds = $seconds - ($years * $secPerYear);

$days = floor($seconds / $secPerDay);

$seconds = $seconds - ($days * $secPerDay);

$hours = floor($seconds / $secPerHour);
$seconds = $seconds - ($hours * $secPerHour);

$minutes = floor($seconds / $secPerMin);
$seconds = $seconds - ($minutes * $secPerMin);



?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Max's Homepage</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css" />

</head>

<body>

<header>
    <?php include('../template/header.php');?>
</header>

<nav>
    <ul>
        <?php include('../template/nav.php'); ?>
    </ul>
</nav>

<main>
    <h2>End of School Countdown</h2>
    <p>Years:<?=$years ?> | Days:<?=$days ?> | Hours:<?=$hours ?> | Minutes:<?=$minutes ?> | Seconds:<?=$seconds ?></p>

<footer>
    <?php include('../template/footer.php'); ?>
</footer>

</body>

</html>
